module Overrides
  class << self

    def included(base)
      override(base)
    end

    def override(klass)
      klass.routes.each do |route|
        headers = {
            "Access-Token": {
                required: true
            },
            "Client": {
                required: true
            },
            "Uid": {
                required: true
            },
            "Expiry": {
                required: false
            },
            "Token-Type": {
                required: false
            }
        }

        options = route.instance_variable_get(:@options)
        options[:headers] ||= {}
        options[:headers].merge!(headers)
        #options[:settings][:description][:headers] ||= {}
        #options[:settings][:description][:headers].merge!(headers)
      end
    end

  end
end