module API
  class Loader < Grape::API
    # before do
    #   header['Access-Control-Allow-Origin'] = '*'
    #   header['Access-Control-Request-Method'] = '*'
    # end

    # logger.formatter = GrapeLogging::Formatters::Default.new
    # use GrapeLogging::Middleware::RequestLogger, { logger: logger }
    # rescue_from :all do |e| RX::API.logger.error e end

    mount V1::Base
    # mount V2::Base

    include Overrides

    base_path = Rails.env.production? ? "#{ENV['URL_SCHEMA']}://#{ENV['URL_HOST']}/api" : "/api"
    add_swagger_documentation base_path: base_path,
                              api_version: 'v1',
                              mount_path: 'docs',
                              hide_format: true,
                              hide_documentation_path: true
  end
end
