module V1
  module Handlers
    def self.included(base)
      base.instance_eval do
        rescue_from Grape::Exceptions::ValidationErrors do |e|
          Rack::Response.new({
                                 error: {
                                     code: 1001,
                                     message: e.message
                                 }
                             }.to_json, e.status)
        end
      end
    end
  end

  class Error < Grape::Exceptions::Base
    attr :code, :text

    def initialize(opts={})
      @code = opts[:code] || 2000
      @text = opts[:text] || ''

      @status = opts[:status] || 400
      @message = {error: {code: @code, message: @text}}
    end
  end

  class UnauthorizedError < Error
    def initialize
      super code: 401, text: "Unauthorized", status: 401
    end
  end

  class NotFoundError < Error
    def initialize(id)
      super code: 404, text: "Record with id##{id} not found", status: 404
    end
  end

  class CardLinkFailedError < Error
    def initialize()
      super code: 400, text: 'Unable to link a card to account by given number and pin', status: 400
    end
  end
  CardLinkFailedError
end
