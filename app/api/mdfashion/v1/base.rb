module V1
  class Base < Grape::API
    version 'v1', using: :path
    format :json
    default_format :json

    include V1::Handlers
    include V1::Helpers
    include V1::Constraints

    include Grape::Kaminari

    before do
      begin
        authenticate_account!
      rescue GrapeDeviseTokenAuth::Unauthorized
        raise UnauthorizedError
      end
    end

    helpers V1::Helpers

    resource :account do
      desc 'Get current account'

      get do
        current_account
      end

      desc 'Subscribe for Push Notification'
      params do
        requires :device_token, type: String, desc: 'Device Token'
        requires :platform, type: String, values: ['iOS', 'Android'], desc: 'Platform: iOS/Android'
      end
      post :subscribe do
        is_subscribed = current_account.subscribers.exists?(device_token: params[:device_token])
        Subscriber.create({
            account: current_account,
            device_token: declared_params[:device_token],
            platform: declared_params[:platform]
                          }) unless is_subscribed
      end

      resource :card do
        desc 'Get the card for an account'
        get do
          card = current_account.card || Card.new
          if card.is_linked? and ( (card.updated_at + 3.minutes ) < Time.now )
            card.refresh
          end
          card
        end

        desc 'Release card assignment'
        delete do
          card = current_account.card || Card.new
          card.delete if card.is_linked?
        end

        desc 'Try to assign given card to account'
        params do
          requires :number, type: String, desc: "Card number"
          requires :pin, type: String, desc: "Card PIN"
        end
        post :link do
          Card.link(current_account, declared_params) or raise CardLinkFailedError
        end
      end

      resource :profile do
        desc 'Get the profile for current account'
        get do
          profile = current_account.profile
          profile || Profile.create(account_id: current_account.id)
        end

        desc 'Update profile data'
        params do
          optional :name, type: String, desc: "User's first name"
          optional :last_name, type: String, desc: "User's last name"
          optional :phone, type: String, desc: "Phone (cellular)"
          optional :address, type: String, desc: "Residential address"
          optional :city, type: String, desc: "City"
          optional :zip, type: String, desc: "ZIP (Postal code)"
        end
        put do
          # p declared_params
          current_account.profile.update(declared_params)
        end
      end
    end

    resource :brands do
      desc 'Get all the brands'
      get do
        Brand.includes(:promotions, :stores).all.map {|brand|
          obj = brand.as_json
          obj['promotions'] = brand.promotions.count
          obj['stores'] = brand.stores.count
          obj
        }
      end

      route_param :id do
        params do
          requires :id, type: Integer, desc: 'Brand Id'
        end
        get do
          Brand.find_by(id: params[:id]) rescue ActiveRecord::RecordNotFound raise NotFoundError, params[:id]
        end
      end
    end

    resource :promotions do
      desc 'Get all the promotions'
      get do
        Promotion.all
      end

      route_param :id do
        params do
          requires :id, type: Integer, desc: 'Promotion Id'
        end
        get do
          Promotion.find_by(id: params[:id]) rescue ActiveRecord::RecordNotFound raise NotFoundError, params[:id]
        end
      end
    end

    resource :stores do
      desc 'Get all the stores'
      get do
        Store.all
      end

      route_param :id do
        params do
          requires :id, type: Integer, desc: 'Store Id'
        end
        get do
          Store.find_by(id: params[:id]) rescue ActiveRecord::RecordNotFound raise NotFoundError, params[:id]
        end
      end
    end

    resource :assets do
      desc 'Get all the assets'
      get do
        images = []
        images.concat Store.all.map { |i| i.image.url }
        images.concat Promotion.all.map { |i| i.image.url }
        images.concat Brand.all.map { |i| i.image.url }
        images
      end

    end

    # resource :statuses do
    #   desc 'Return a public timeline.'
    #   get :public_timeline do
    #     Status.limit(20)
    #   end
    #
    #   desc 'Return a personal timeline.'
    #   get :home_timeline do
    #     authenticate!
    #     current_user.statuses.limit(20)
    #   end
    #
    #   desc 'Return a status.'
    #   params do
    #     requires :id, type: Integer, desc: 'Status id.'
    #   end
    #   route_param :id do
    #     get do
    #       Status.find(params[:id])
    #     end
    #   end
    #
    #   desc 'Create a status.'
    #   params do
    #     requires :status, type: String, desc: 'Your status.'
    #   end
    #   post do
    #     authenticate!
    #     Status.create!({
    #                        user: current_user,
    #                        text: params[:status]
    #                    })
    #   end
    #
    #   desc 'Update a status.'
    #   params do
    #     requires :id, type: String, desc: 'Status ID.'
    #     requires :status, type: String, desc: 'Your status.'
    #   end
    #   put ':id' do
    #     authenticate!
    #     current_user.statuses.find(params[:id]).update({
    #                                                        user: current_user,
    #                                                        text: params[:status]
    #                                                    })
    #   end
    #
    #   desc 'Delete a status.'
    #   params do
    #     requires :id, type: String, desc: 'Status ID.'
    #   end
    #   delete ':id' do
    #     authenticate!
    #     current_user.statuses.find(params[:id]).destroy
    #   end
    # end

  end
end