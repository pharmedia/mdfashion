module V1
  module Helpers
    extend Grape::API::Helpers
    include GrapeDeviseTokenAuth::AuthHelpers

    params :token do
      optional :token, type: String, desc: "Session Token"
    end

    params :location do
      optional :location, type: Hash do
        requires :city_id, type: Integer, desc: 'City id'
        optional :address, type: String, desc: 'Full address for geo-coding'
      end

      optional :coordinates, type: Hash do
        requires :latitude, type: Float, desc: 'Latitude'
        requires :longitude, type: Float, desc: 'Longitude'
      end

      optional :distance, type: Integer, default: 5, values: [1, 2, 5, 10]

      mutually_exclusive :location, :coordinates
    end

    params :pagination do
      optional :page, type: Integer, default: 1, values: 1..9,
               desc: 'Page offset to fetch.'
      optional :per_page, type: Integer, default: Kaminari.config.default_per_page,
               desc: 'Number of results to return per page.',
               values: [10, 25, 50]
    end

    def headers(name, &block)
      Grape::DSL::Configuration.desc_container.configure do
        headers [
                    "Access-Token": {
                        required: true
                    },
                    "Uid": {
                        required: true
                    },
                    "Expiry": {
                        required: false
                    },
                    "Token-Type": {
                        required: false
                    },
                    "Client": {
                        required: false
                    }]
      end
    end

    def paginate_array(array)
      Kaminari.paginate_array(array)
          .page(params[:page])
          .per(params[:per_page])
          .padding(params[:offset]).tap do |data|
        header "X-Total", data.total_count.to_s
        header "X-Total-Pages", data.num_pages.to_s
        header "X-Per-Page", params[:per_page].to_s
        header "X-Page", data.current_page.to_s
        header "X-Next-Page", data.next_page.to_s
        header "X-Prev-Page", data.prev_page.to_s
      end
    end

    def declared_params
      declared(params, include_missing: false)
    end
  end
end
