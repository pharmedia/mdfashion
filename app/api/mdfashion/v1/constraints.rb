module V1
  module Constraints
    class << self

      def included(base)
        apply_rules!
        base.use Rack::Attack
      end

      def apply_rules!
        Rack::Attack.blacklist('Block api access from other ip if trusted ip set') do |req|
          req.env['api.token'] && !req.env['api.token'].allow_ip?(req.ip)
        end

        Rack::Attack.throttle('Authorized access', limit: 300, period: 5.minutes) do |req|
          req.env['api.token'] && req.env['api.token'].access_key
        end
      end

    end
  end
end