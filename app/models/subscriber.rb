class Subscriber < ActiveRecord::Base
  belongs_to :account

  validates :account, :device_token, :platform, presence: true
end
