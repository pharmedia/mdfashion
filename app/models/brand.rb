class Brand < ActiveRecord::Base
  has_many :promotions
  has_many :stores

  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  attr_accessor :delete_image
  before_validation { self.asset.clear if self.delete_image == '1' }

  has_paper_trail

  validates :title, :subtitle, :logo, :content, :image, presence: true
end
