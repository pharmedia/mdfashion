class Notification < ActiveRecord::Base
  validates :title, :content, :payload, presence: true
  # validates :platform, format: {with: /iOS|Android/}, allow_blank: true, allow_nil: true

  after_create do
    deliver
  end

  private
  def deliver
    notify_ios unless self.platform == 'Android'
    notify_android unless self.platform == 'iOS'

    Rpush.push
  end

  def notify_ios
    subscribers = self.device_token.empty? ?
        Subscriber.where(platform: 'iOS') :
        Subscriber.where(device_token: self.device_token, platform: 'iOS')

    subscribers.each do |s|
      n = Rpush::Apns::Notification.new
      n.app = Rpush::Apns::App.find_by_name("MD Fashion iOS Production X2")
      n.device_token = s.device_token
      n.alert = self.content
      # n.data = { body: :bar }
      n.save!
    end
  end

  def notify_android
    subscribers = self.device_token.empty? ?
        Subscriber.where(platform: 'Android') :
        Subscriber.where(device_token: self.device_token, platform: 'Android')

    subscribers.each do |s|
      n = Rpush::Gcm::Notification.new
      n.app = Rpush::Gcm::App.find_by_name("MD Fashion Android")
      n.registration_ids = [ s[:device_token] ]
      n.data = { message: self.content }
      n.priority = 'high'        # Optional, can be either 'normal' or 'high'
      n.content_available = true # Optional
      # Optional notification payload. See the reference below for more keys you can use!
      n.notification = { body: s[:payload],
                         title: s[:title],
                         # icon: 'myicon'
      }
      n.save!
    end
  end
end
