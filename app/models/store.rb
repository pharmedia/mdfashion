class Store < ActiveRecord::Base
  belongs_to :brand

  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  has_paper_trail

  validates :title, :subtitle, :description, :work_hours,
            :address, :phone, :latitude, :longitude, presence: true
end
