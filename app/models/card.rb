class Card < ActiveRecord::Base
  belongs_to :account

  validates :number, :pin, :account, presence: true

  def refresh
    callback = Card.lookup self
    balance = callback.try(:balance)
    update_attribute(:balance, balance) && touch if balance
  end

  def self.link(account, params)
    card = find_or_initialize_by(account_id: account.id)

    attributes = {
        number: params[:number],
        pin: params[:pin],
        is_linked: false
    }

    callback = lookup(params)
    is_assigned = callback ? ( callback["status"] == "assigned") : false

        attributes.merge!({
        phone: callback["data"]["phone"],
        name: callback["data"]["name"],
        rating: callback["data"]["kind"],
        balance: callback["balance"],
        is_linked: true
                     }) if is_assigned

    card.update(attributes)

    is_assigned
  end

  def self.lookup(params)
    card_link = Curl::Easy.http_post("http://92.60.177.73:7000/Webservice/hs/md_api/link_discount_card", "{}") do |c|
      c.http_auth_types = :basic
      c.username = 'UserForAutoExchange'
      c.password = '123321'
      c.headers["User-Agent"] = "MDFashion Interconnect for Mobile/0.1"
      c.headers["Content-Type"] = "application/json"
      c.headers["Accept"] = "application/json"
      c.headers["X-Pin"] = params[:pin]
      c.headers["X-Card-Number"] = params[:number]
    end

    begin
      JSON.parse(card_link.body_str)
    rescue JSON::ParserError => e
      nil
    end if card_link

  end
end
