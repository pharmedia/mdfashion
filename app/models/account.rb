class Account < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable, :timeoutable, :omniauthable

  include DeviseTokenAuth::Concerns::User

  has_paper_trail

  has_one :profile, dependent: :destroy
  has_one :card, dependent: :destroy
  has_many :subscribers
end