class Accounts::SessionsController < DeviseTokenAuth::SessionsController
  def facebook
    begin
      graph = Koala::Facebook::API.new(facebook_params[:token], ENV['FACEBOOK_SECRET'])
      fb = graph.get_object("me")
    rescue Koala::Facebook::AuthenticationError => e
      raise e
    rescue Koala::Facebook::ClientError => e
      raise e
    else
      email = fb['email'] || "#{fb['id']}@facebook.com"
      account = Account.where(email: email).first_or_initialize.tap do |account|
        account.provider = 'facebook'
        account.uid = fb['id']
        account.oauth_token = facebook_params[:token]
        account.email = email
        account.password = Devise.friendly_token[0,20]
        account.confirmed_at = Time.now
        # account.tokens[SecureRandom.urlsafe_base64(nil, false)] = {
        #     token: BCrypt::Password.create(SecureRandom.urlsafe_base64(nil, false)),
        #     expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
        # }
      end

      unless account.persisted?
        account.save!(validate: false)

        account.profile = Profile.create({
                                             nickname: fb['name'],
                                             first_name: fb['first_name'],
                                             last_name: fb['last_name'],
                                             birthday: fb['birthday'],
                                             # location: fb['location']['name'],
                                             uid: fb['id'],
                                             oauth_token: facebook_params[:token]
                                         })
      end

      auth_headers = account.create_new_auth_token
      response.headers.merge!(auth_headers)

      sign_in(:account, account, store: false, bypass: false)

      render json: {
                 data: account.token_validation_response
             }
    end
  end

  private
  def facebook_params
    params.permit(:token)
  end
end