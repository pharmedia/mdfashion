class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    account = Account.from_omniauth(request.env["omniauth.auth"])

    if account.persisted?
      sign_in account
    else
      begin
        graph = Koala::Facebook::API.new(auth.credentials.token, ENV['FACEBOOK_SECRET'])
        fb = graph.get_object("me")
      rescue Koala::Facebook::AuthenticationError => e
        # Handle this shit
      else
        account.email = fb['email'] || "#{fb['id']}@facebook.com"
        account.password = Devise.friendly_token[0,20]
        account.confirmed_at = Time.now
        account.save

        account.profile = Profile.create({
                                             nickname: fb['name'],
                                             first_name: fb['first_name'],
                                             last_name: fb['last_name'],
                                             birthday: fb['birthday'],
                                             # location: fb['location']['name'],
                                             uid: fb['id'],
                                             oauth_token: facebook_params[:token]
                                         })

        auth_headers = account.create_new_auth_token
        response.headers.merge!(auth_headers)

        sign_in(:account, account, store: false, bypass: false)
      end
    end

  end

  def failure
    super
  end

  def google_oauth2
  end

  private
  def facebook_params
    params.permit(:token)
  end

end