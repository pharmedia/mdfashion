class CreateSubscribers < ActiveRecord::Migration
  def change
    create_table :subscribers do |t|
      t.string :device_token, index: true
      t.string :platform
      t.belongs_to :account, index: true

      t.timestamps null: false
    end
  end
end
