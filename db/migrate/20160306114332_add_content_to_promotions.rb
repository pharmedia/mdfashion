class AddContentToPromotions < ActiveRecord::Migration
  def change
    add_column :promotions, :content, :text
  end
end
