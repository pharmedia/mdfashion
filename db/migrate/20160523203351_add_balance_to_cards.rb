class AddBalanceToCards < ActiveRecord::Migration
  def change
    add_column :cards, :balance, :string, default: 0
  end
end
