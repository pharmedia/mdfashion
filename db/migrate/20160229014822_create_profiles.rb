class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :last_name
      t.string :address
      t.string :city
      t.string :zip

      t.references :account, index: true

      t.timestamps null: false
    end
  end
end
