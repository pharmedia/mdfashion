class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.string :title
      t.string :subtitle
      t.boolean :is_featured
      t.string :call_to_action

      t.references :brand, index: true

      t.timestamps null: false
    end
  end
end
