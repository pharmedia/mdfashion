class AddRatingToCards < ActiveRecord::Migration
  def change
    add_column :cards, :rating, :string, default: '10%'
  end
end
