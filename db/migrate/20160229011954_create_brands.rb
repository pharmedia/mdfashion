class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :title
      t.string :subtitle
      t.text :content
      t.text :logo

      t.timestamps null: false
    end
  end
end
