class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :title
      t.string :subtitle
      t.text :description
      t.string :work_hours
      t.string :phone
      t.string :address
      t.string :latitude
      t.string :longitude

      t.references :brand, index: true

      t.timestamps null: false
    end
  end
end
