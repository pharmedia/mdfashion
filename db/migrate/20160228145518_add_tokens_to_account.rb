class AddTokensToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :uid, :string, null: false, index: true
    add_column :accounts, :provider, :string, null:false, default: 'email', index: true
    add_column :accounts, :authentication_token, :string, index: true, unique: true
    add_column :accounts, :oauth_token, :string, index: true
    add_column :accounts, :tokens, :text

    add_index :accounts, [:uid, :provider], unique: true
  end
end
