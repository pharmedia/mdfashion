class AddIsLinkedToCards < ActiveRecord::Migration
  def change
    add_column :cards, :is_linked, :boolean
  end
end
