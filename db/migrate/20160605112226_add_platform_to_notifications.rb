class AddPlatformToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :platform, :string, index: true
  end
end
