class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :title
      t.string :content
      t.text :payload
      t.string :device_token

      t.timestamps null: false
    end
  end
end
