RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :account
  # end
  # config.current_user_method(&:current_account)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  config.audit_with :paper_trail, 'Account', 'PaperTrail::Version'

  config.main_app_name = %w{MD Fashion}

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard # mandatory
    index # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    history_index
    history_show

    nestable
  end

  config.included_models = %w(Account Brand Card Notification Profile Promotion Store Subscriber)

  config.model 'Account' do
      navigation_label 'Учетные записи'
      label 'Аккаунт'
      label_plural 'Аккаунты'
  end

  config.model 'Profile' do
    parent Account
    label 'Профайл'
    label_plural 'Профайлы'

    field :name do
      label 'Имя'
    end

    field :last_name do
      label 'Фамилия'
    end

    field :phone do
      label 'Телефон'
    end

    field :address do
      label 'Адрес'
    end

    field :city do
      label 'Город'
    end

    field :zip do
      label 'Индекс'
    end

    field :account do
      label 'К какому аккаунту относится'
    end
  end

  config.model 'Card' do
    parent Account
    label 'Карта'
    label_plural 'Карты'

    field :number do
      label 'Номер карты'
    end

    field :pin do
      label 'PIN-код'
    end

    field :balance do
      label 'Балланс по карте'
    end

    field :rating do
      label 'Тип карты (10%, 15%, 20%)'
    end

    field :phone do
      label 'Связанный телефон'
    end

    field :is_linked do
      label 'Карта авторизована?'
    end

    field :account do
      label 'К какому аккаунту относится'
    end
  end

  config.model 'Brand' do
    label 'Бренд'
    label_plural 'Бренды'


    field :title do
      label 'Название'
    end

    field :subtitle do
      label 'Подзаголовок (слоган)'
    end

    field :content, :ck_editor do
      label 'История'
    end

    field :logo do
      label 'Логотип (SVG)'
    end

    field :image, :paperclip do
      label 'Подложка (фотография)'
    end
  end

  config.model 'Store' do
    parent Brand
    label 'Магазин'
    label_plural 'Магазины'

    field :title do
      label 'Название'
    end

    field :subtitle do
      label 'Подзаголовок'
    end

    field :brand do
      label 'К какому бренду относится'
    end

    field :description, :ck_editor do
      label 'О магазине'
    end

    field :image, :paperclip do
      label 'Изображение'
    end

    field :work_hours do
      label 'Часы работы'
    end

    field :phone do
      label 'Телефон(ы)'
    end

    field :address do
      label 'Адрес (включая город)'
    end

      field :longitude do
        label 'Координаты (lon)'
      end

      field :latitude do
        label 'Координаты (lat)'
      end

  end

  config.model 'Promotion' do
    parent Brand
    label 'Акция'
    label_plural 'Акции'

    field :title do
      label 'Название'
    end

    field :subtitle do
      label 'Подзаголовок (слоган)'
    end

    field :is_featured do
      label 'Показывать в слайдере?'
    end

    field :call_to_action do
      label 'CTA-фраза (кнопка)'
    end

    field :brand do
      label 'К какому бренду относится'
    end

    field :image, :paperclip do
      label 'Изображение'
    end

    field :content, :ck_editor do
      label 'Текст акции'
    end
  end

  config.model 'Notification' do
    label 'Сообщение PUSH'
    label_plural 'Сообщения PUSH'

    field :title do
      label 'Заголовок'
    end

    field :content do
      label 'Содержимое PUSH'
    end

    field :payload do
      label 'Текст'
    end

    field :platform do
      label 'Платформа'
    end

    field :device_token do
      label 'Токен получателя'
    end
  end

  config.model 'Subscriber' do
    parent 'Notification'
    label 'Подписчик'
    label_plural 'Подписчики'

    field :account do
      label 'Учетная запись'
    end

    field :device_token do
      label 'Токен'
    end

    field :platform do
      label 'Платформа (iOS/Android)'
    end
  end

end
