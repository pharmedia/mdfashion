require File.expand_path('../boot', __FILE__)

require 'rack/cors'
require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MDFashion
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.i18n.default_locale = :ru

    config.encoding = 'utf-8'
    config.active_support.escape_html_entities_in_json = true

    config.time_zone = 'Kyiv'
    config.active_record.default_timezone = :local

    config.paths.add File.join('app', 'api'), glob: File.join('**', '*.rb')
    config.autoload_paths += Dir[Rails.root.join('app', 'api', '*')]

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    config.active_job.queue_adapter = :sidekiq

    # config.middleware.insert_before 0, "Rack::Cors" do
    #   allow do
    #     origins ''
    #     resource '*',
    #              :headers => :any,
    #              :expose  => ['access-token', 'expiry', 'token-type', 'uid', 'client'],
    #              :methods => [:get, :post, :options, :delete, :put]
    #   end
    # end

    # config.middleware.use Rack::Cors do
    #   allow do
    #     origins ''
    #     resource '*',
    #              :headers => :any,
    #              :expose  => ['access-token', 'expiry', 'token-type', 'uid', 'client'],
    #              :methods => [:get, :post, :options, :delete, :put]
    #   end
    # end
  end
end


Rails.application.routes.default_url_options[:host] = 'mdfashion.com.ua'
